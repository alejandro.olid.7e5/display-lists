package com.example.affirmations
import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.Icon
import androidx.compose.material.IconButton
import androidx.compose.material.MaterialTheme
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Email
import androidx.compose.material.icons.filled.Share
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import coil.compose.rememberImagePainter
import com.example.affirmations.ui.theme.AffirmationsTheme
import java.util.ArrayList

class DetailsActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            AffirmationsTheme {
                val affirmation = intent.getStringArrayExtra("array")
                if (affirmation != null) {
                    Details(affirmation)
                }
                else{
                    DetailsNull()
                }
            }
        }
    }

    private fun Context.sendMail(to:String, subject: String){
        try {
            val intent = Intent(Intent.ACTION_SEND)
            intent.type = "message"
            intent.putExtra(Intent.EXTRA_EMAIL, arrayOf(to))
            intent.putExtra(Intent.EXTRA_SUBJECT, subject)
            startActivity(intent)
        } catch (_: ActivityNotFoundException) { }
        catch (_: Throwable) {}
    }

    private fun Context.dial(phone: String) {
        try {
            val intent = Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", phone, null))
            startActivity(intent)
        } catch (_: Throwable) { }
    }


    @Composable
    private fun DetailsNull() {
        Column(
            horizontalAlignment = Alignment.CenterHorizontally,
            modifier = Modifier
                .fillMaxSize()
                .background(color = Color.LightGray)
        ) {Text(
            text = "Informacion no recibida",
            modifier = Modifier.padding(16.dp),
            style = MaterialTheme.typography.h6
        )
        }
    }

@Composable
fun Details(affirmation: Array<String>) {
    val context = LocalContext.current
    Column(
        horizontalAlignment = Alignment.CenterHorizontally,
        modifier = Modifier
            .fillMaxSize()
            .background(color = Color.LightGray)
            .verticalScroll(rememberScrollState())
    ) {
        Image(
            painter = rememberImagePainter(
                data = affirmation[2],
                builder = {
                    crossfade(true)
                }
            ),
                contentDescription = "Imagen desde URL"
            )
        Text(
            text = affirmation[0],
            modifier = Modifier.padding(16.dp),
            style = MaterialTheme.typography.h6
        )
        Text(
            text = affirmation[1],
            modifier = Modifier.padding(16.dp),
            style = MaterialTheme.typography.h6
        )
        IconButton(
            onClick = { context.sendMail("", "")},
            modifier = Modifier.padding(8.dp)
        ) {
            Icon(
                imageVector = Icons.Default.Email,
                contentDescription = "Botón compartir"
            )
        }
        IconButton(
            onClick = { context.dial("")},
            modifier = Modifier.padding(8.dp)
        ) {
            Icon(
                imageVector = Icons.Default.Share,
                contentDescription = "Botón compartir"
            )
        }
    }
}
    @Preview
    @Composable
    fun GreetingPreview() {
        AffirmationsTheme {
            val arrayList = arrayOf("papi juancho", "lambebichu", "image url")
            Details(arrayList)
        }
    }
}

